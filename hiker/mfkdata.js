const  mfkys = {
    getParStr: function(index){
        let listRule = this.url_array[index].listRule;
        return [listRule.列表定位, listRule.元素标题,listRule.图片,listRule.元素描述,listRule.详细地址].join(";");
    },
    url_array: [
        {
            'url': "http://qzy998-1.ysgq.xyz",
            'parseRule': {
                '分类颜色': '#098AC1',
                '大类定位': 'body&&.filter_line',
                '拼接分类': '',
                '小类定位': 'body&&a',
                '分类标题': 'a&&Text',
                '分类链接': 'a&&href',
            },
            'searchRule': {
                '列表': '.ec-search&&.search-list',
                '标题': '.right&&a&&Text',
                '描述': '.ecitem-desc&&Text',
                '类型': '',
                '简介': '.ecitem-desc&&Text',
                '图片': '.eclazy&&data-original',
                '链接': '.playbtn&&a&&href',
            },
            'erjiParseRule': {
                '线路列表': 'body&&.channelname',
                '线路标签': 'a&&Text',
                '选集列表': '.content_playlist&&ul',
                '选集标签': 'li',
                '类型': '.item-director&&a&&Text',
                '演员': '.item-desc,1&&a&&Text',
                '更新': '.item-desc,0&&Text',
                '导演': '.item-desc,2&&Text',
                '图片': '.g-playicon&&img&&src',
                '图片链接': '',
                '简介': '.item-desc,3&&span&&Text'
            },
            'listRule': {
                '列表定位': 'body&&.pack-ykpack',
                '元素标题': 'a&&title',
                '元素描述': 'a&&span,-1&&Text',
                '图片': '.eclazy&&data-original',
                '详细地址': 'a&&href',
            }
		},
        {
            'url': 'http://qzy998-m-1.ysgq.xyz',
            'parseRule': {
                '分类颜色': '#098AC1',
                '大类定位': 'body&&.library-box',
                '拼接分类': '',
                '小类定位': 'body&&a',
                '分类标题': 'a&&Text',
                '分类链接': 'a&&href',
            },
            'searchRule': {
                '列表': '.module-items&&.module-search-item',
                '标题': '.video-info-header&&a&&title',
                '描述': '.video-info-item,-1&&Text',
                '类型': 'p&&Text',
                '简介': '.video-tag-icon&&Text',
                '图片': '.lazyload&&data-src',
                '链接': 'a&&href',
            },
            'erjiParseRule': {
                '线路列表': 'body&&.module-tab-item',
                '线路标签': 'div&&span&&Text',
                '选集列表': 'body&&.sort-item',
                '选集标签': 'div&&a',
                '类型': '.video-tag-icon&&Text',
                '演员': '.video-info-actor,1&&a&&Text',
                '更新': '.video-info-item,3&&Text',
                '导演': '.video-info-item,0&&Text',
                '图片': '.lazyload&&data-src',
                '图片链接': '',
                '简介': '.vod_content&&span&&Text'
            },
            'listRule': {
                '列表定位': 'body&&.module-item-pic',
                '元素标题': 'a&&title',
                '元素描述': '.module-item-style&&Text',
                '图片': 'img&&data-src',
                '详细地址': 'a&&href',
            }
		},
        {
            'url': 'http://qzy998-h-1.ysgq.xyz',
            'parseRule': {
                '分类颜色': '#098AC1',
                '大类定位': 'body&&.library-box',
                '拼接分类': '',
                '小类定位': 'body&&a',
                '分类标题': 'a&&Text',
                '分类链接': 'a&&href',
            },
            'searchRule': {
                '列表': '.module-items&&.module-search-item',
                '标题': '.video-info-header&&a&&title',
                '描述': '.video-info-item,-1&&Text',
                '类型': 'p&&Text',
                '简介': '.video-tag-icon&&Text',
                '图片': '.lazyload&&data-src',
                '链接': 'a&&href',
            },
            'erjiParseRule': {
                '线路列表': 'body&&.module-tab-item',
                '线路标签': 'div&&span&&Text',
                '选集列表': 'body&&.sort-item',
                '选集标签': 'div&&a',
                '类型': '.video-tag-icon&&Text',
                '演员': '.video-info-actor,1&&a&&Text',
                '更新': '.video-info-item,3&&Text',
                '导演': '.video-info-item,0&&Text',
                '图片': '.lazyload&&data-src',
                '图片链接': '',
                '简介': '.vod_content&&span&&Text'
            },
            'listRule': {
                '列表定位': 'body&&.module-item-pic',
                '元素标题': 'a&&title',
                '元素描述': '.module-item-style&&Text',
                '图片': 'img&&data-src',
                '详细地址': 'a&&href',
            }
		}
	]
}
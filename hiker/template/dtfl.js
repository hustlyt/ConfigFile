//hiker://page/dtfl
//拷贝自墙佬的模板Q
function dtfl(d, html, parseRule, page) {
    const empty = "hiker://empty"
    const 分类颜色 = parseRule.分类颜色;
    const 大类定位 = parseRule.大类定位;
    const 拼接分类 = parseRule.拼接分类;
    const 小类定位 = parseRule.小类定位;
    const 分类标题 = parseRule.分类标题;
    const 分类链接 = parseRule.分类链接;
    log("分类标题" + 分类标题);
    log("分类标题" + 分类标题)
    try {
        var categories = pdfa(html, 大类定位).concat(pdfa(html, 拼接分类));
    } catch (e) {
        var categories = pdfa(html, 大类定位);
    }

    let init_cate = []

    for (let i = 0; i < 20; i++) {
        init_cate.push("0")
    }

    const fold = getVar(MY_RULE.group, "0")
    const cate_temp_json = getVar(MY_RULE.title, JSON.stringify(init_cate))
    const cate_temp = JSON.parse(cate_temp_json)

    if (parseInt(page) === 1) {
        d.push({
            title: fold === '1' ? '““””<b><span style="color: #FF0000">∨</span></b>' : '““””<b><span style="color: #1aad19">∧</span></b>',
            url: $().lazyRule((fold) => {
                putVar(MY_RULE.group, fold === '1' ? '0' : '1');
                refreshPage(false);
                return "hiker://empty"
            }, fold),
            col_type: 'scroll_button',
        })


        categories.forEach((category, index) => {
            let sub_categories = pdfa(category, 小类定位);
            if (index === 0) {
                sub_categories.forEach((item, key) => {
                    let title = pdfh(item, 分类标题)
                    d.push({
                        title: key.toString() === cate_temp[index] ?
                            '““””<b><span style="color: ' + 分类颜色 + '">' + title + '</span></b>' :
                            title,
                        url: $(parseDom(item, 分类链接)).lazyRule((params) => {
                            let new_cate = []
                            params.cate_temp.forEach((cate, index) => {
                                new_cate.push(index === 0 ? params.key.toString() :
                                    "0")
                            })
                            putVar(MY_RULE.title, JSON.stringify(new_cate))
                            putVar(MY_RULE.url, input)
                            refreshPage(true)
                            return "hiker://empty"
                        }, {
                            cate_temp: cate_temp,
                            key: key,
                            page: page,
                        }),
                        col_type: 'scroll_button',
                    })
                })
                d.push({
                    col_type: "blank_block"
                });
            } else if (fold === '1') {
                sub_categories.forEach((item, key) => {
                    let title = pdfh(item, 分类标题)
                    d.push({
                        title: key.toString() === cate_temp[index] ?
                            '““””<b><span style="color: ' + 分类颜色 + '">' + title + '</span></b>' :
                            title,
                        url: $(parseDom(item, 分类链接)).lazyRule((params) => {
                            params.cate_temp[params.index] = params.key.toString()

                            putVar(MY_RULE.title, JSON.stringify(params.cate_temp))
                            putVar(MY_RULE.url, input)
                            refreshPage(true)
                            return "hiker://empty"
                        }, {
                            cate_temp: cate_temp,
                            index: index,
                            key: key,
                            page: page,
                        }),
                        col_type: 'scroll_button',
                    })
                })
                d.push({
                    col_type: "blank_block"
                });
            }
        })
    }
}